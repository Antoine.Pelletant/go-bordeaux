package main

type MyMap struct {
	Cycles	int64
	Size    Vector
}

func NewMyMap(cycles int64, size Vector) MyMap {
	return MyMap{
		Cycles: cycles,
		Size: size,
	}
}

func (p MyMap) GetSize() Vector {
	return p.Size
}

func (p MyMap) getCycles() int64 {
	return p.Cycles
}
