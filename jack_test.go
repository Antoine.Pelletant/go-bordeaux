package main

import (
	"testing"
)

type testJackPosition struct {
	name   string
	jacks  Jack
	expect Point
}

func TestJack_IsOccupied(t *testing.T) {
	tests := []testJackPosition{
		{
			name:   "Test blocked",
			jacks:  NewJack("jack1", Point{0, 0}),
			expect: Point{0, 0},
		},
		{
			name:   "Test blocked",
			jacks:  NewJack("jack1", Point{3, 3}),
			expect: Point{3, 3},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			x, y := test.jacks.GetPosition()
			if x != test.expect.X || y != test.expect.Y {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.jacks.Position)
			}
		})
	}
}
