package main

import (
	"fmt"
	"math"
)

type goToType int

const (
	NONE goToType = iota
	DOCKER
	PARCELER
)

//Jacj Definition of Jack structure
type Jack struct {
	Name     string
	Position Point
	Parcel   *Parceller
	GoToPos  *Vector
	GoToType goToType
}

//NewJack Create a new Jacl
func NewJack(name string, position Point) Jack {
	return Jack{
		Name:     name,
		Position: position,
		Parcel:   nil,
		GoToPos:  nil,
		GoToType: NONE,
	}
}

func isBlocked(nextPos Vector, obs *[]Vector) bool {
	if obs == nil {
		return false
	}
	for _, v := range *obs {
		if v.X == nextPos.X && v.Y == nextPos.Y {
			return true
		}
	}
	return false
}

//Tick execute next jack action
func (j *Jack) Tick(obs *[]Vector, wareHouseSize Vector) {

	if j.GoToType == NONE {
		fmt.Printf("%s WAITING\n", j.Name)
	} else {
		nextPos := GetNextPos(wareHouseSize, j.Position.ToVector(), *j.GoToPos)

		if nextPos != nil {
			if isBlocked(*nextPos, obs) == true {
				fmt.Printf("%s WAITING\n", j.Name)
			} else {
				j.Position = (*nextPos).VectorToPoint()
				fmt.Printf("%s GO [%d,%d]\n", j.Name, j.Position.X, j.Position.Y)
			}
		}
	}
}

func (j Jack) IsOccupied() bool {
	if j.GoToType == NONE {
		return false
	}
	return true
}

func (j Jack) GetPosition() (x, y uint) {
	return uint(j.Position.X), uint(j.Position.Y)
}

//GoTo define where to go and what to do
func (j *Jack) GoTo(pos *Vector, goToType goToType) {
	j.GoToType = goToType
	j.GoToPos = pos
}

//IsArrived return true is jack is at final destination false if not
func (j Jack) IsArrived() bool {
	if j.GoToPos == nil {
		return false
	}

	goTo := j.GoToPos
	pos := j.Position.ToVector()
	if math.Abs(goTo.X - pos.X) == 1 && math.Abs(goTo.Y - pos.Y) == 0 {
		return true
	} else if math.Abs(goTo.X - pos.X) == 0 && math.Abs(goTo.Y - pos.Y) == 1 {
		return true
	}
	return false
}

//IsEmpty return true if jack is empty false if not
func (j Jack) IsEmpty() bool {
	if j.Parcel == nil {
		return true
	}
	return false
}

//Pick pick-up parcel
func (j *Jack) Pick(parcel Parceller) {
	j.Parcel = &parcel
	fmt.Printf("%s TAKE %s %s\n", j.Name, (*j.Parcel).GetName(), (*j.Parcel).GetColor())
}

func (j *Jack) Drop() Parceller {
	fmt.Printf("%s LEAVE %s %s\n", j.Name, (*j.Parcel).GetName(), (*j.Parcel).GetColor())
	p := j.Parcel

	j.Parcel = nil
	j.GoToType = NONE
	j.GoToPos = nil

	return *p
}
