package main

import (
	"testing"
)

type testPath struct {
	name   string
	start  Vector
	end    Vector
	expect []Vector
	whSize Vector
}

func TestPath_GetPath(t *testing.T) {
	tests := []testPath{
		{
			name:   "Test long path",
			start:  Vector{0, 0},
			end:    Vector{4, 4},
			expect: []Vector{{0, 1}, {1, 1}, {1, 2}, {2, 2}, {2, 3}, {3, 3}, {3, 4}, {4, 4}},
			whSize: Vector{5, 5},
		}, {
			name:   "Test short path",
			start:  Vector{0, 0},
			end:    Vector{1, 1},
			expect: []Vector{{0, 1}, {1, 1}},
			whSize: Vector{5, 5},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res := Getpath(test.whSize, test.start, test.end)
			i := 0
			for i < len(res) {
				if res[i] != test.expect[i] {
					t.Errorf("Test failed expecting %v got %v", test.expect[i], res[i])
				}
				i++
			}
		})
	}
}
