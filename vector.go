package main

import (
	"math"
)

type Vector struct {
	X, Y float64
}

func VectorFromInts(X, Y uint) Vector {
	return Vector{X: float64(X), Y: float64(Y)}
}

func VectorFromInt(X, Y int64) Vector {
	return Vector{X: float64(X), Y: float64(Y)}
}

func (v Vector) VectorToPoint() Point {
	return PointFromFloats(v.X, v.Y)
}

// This function simply add a float to both coordinates of the vector v
func (v *Vector) Add(a float64) {
	v.X += a
	v.Y += a
}

// This function simply add the values of another vector a for both X and Y
func (v *Vector) AddVector(a Vector) {
	v.X += a.X
	v.Y += a.Y
}

// This function simply subtract a float to both coordinates of the vector v
func (v *Vector) SubtractVector(a Vector) {
	v.X -= a.X
	v.Y -= a.Y
}

// This function simply subtract the values of another vector a for both X and Y
func (v *Vector) Subtract(a float64) {
	v.X -= a
	v.Y -= a
}

// This function simply subtract the values of another vector a for both X and Y
func (v *Vector) MultiplyVector(a Vector) {
	v.X *= a.X
	v.Y *= a.Y
}

// This function simply multiply a float to both coordinates of the vector v
func (v *Vector) Multiply(a float64) {
	v.X *= a
	v.Y *= a
}

// This function simply subtract the values of another vector a for both X and Y
func (v *Vector) DivideVector(a Vector) {
	if a.X != 0 {
		v.X /= a.X
	}
	if a.Y != 0 {
		v.Y /= a.Y
	}
}

// This function simply divide a float to both coordinates of the vector v
func (v *Vector) Divide(a float64) {
	if a != 0 {
		v.X /= a
		v.Y /= a
	}
}

// This function returns the dot product of a vector
// More information about what is a dot product here https://en.wikipedia.org/wiki/Dot_product
func (v Vector) DotProduct(a Vector) float64 {
	return v.X*a.X + v.Y*a.Y
}

// This function returns the cross product of a vector
// More information about what is a dot product here https://en.wikipedia.org/wiki/Cross_product
func (v Vector) CrossProduct(a Vector) float64 {
	return v.X*a.Y - v.Y*a.X
}

// This function returns the length of a vector. The length is the defined like the hypotenuse of a vector
// Example for the vector (3;4) :
// length = sqrt(X² + Y²)
//        = sqrt(3² + 4²)
// 		  = sqrt(25)
// length = 5
func (v Vector) Length() float64 {
	return math.Sqrt(v.DotProduct(v))
}

// This function normalize the vector meaning it will set his length to 1 keeping the same inclination.
// This can simply be done by divining the vector by his length.
func (v *Vector) Normalize() {
	v.Divide((*v).Length())
}

// This function return the minimal value between X and Y
func (v Vector) Min() float64 {
	return math.Min(v.X, v.Y)
}

// This function return the maximal value between X and Y
func (v Vector) Max() float64 {
	return math.Max(v.X, v.Y)
}

// This function returns the angle (the slope) of the vector in radians
func (v Vector) ToAngleRad() float64 {
	return -math.Atan2(-v.Y, v.X)
}

// This function returns the angle (the slope) of the vector in degrees
func (v Vector) ToAngleDeg() float64 {
	return v.ToAngleRad() * 180 / math.Pi
}

// This function returns the angle (the slope) of the vector in degrees absolute (between 0 and 360)
func (v Vector) ToAngleDegAbs() float64 {
	ret := math.Mod(v.ToAngleDeg(), 360)
	if ret < 0 {
		return ret + 360
	}
	return ret
}

// This function returns the angle (the slope) of two vectors in radians
func (v Vector) AngleToRad(a Vector) float64 {
	a.SubtractVector(v)
	return a.ToAngleRad()
}

// This function returns the angle (the slope) of two vectors in degrees (from -180 to 180)
func (v Vector) AngleToDeg(a Vector) float64 {
	a.SubtractVector(v)
	return a.ToAngleDeg()
}

// This function returns the angle (the slope) of two vectors in degrees absolute (from 0 to 360)
func (v Vector) AngleToDegAbs(a Vector) float64 {
	a.SubtractVector(v)
	return a.ToAngleDegAbs()
}

// This function returns the distance to a
func (v Vector) DistanceTo(a Vector) float64 {
	a.SubtractVector(v)
	return a.Length()
}
