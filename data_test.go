package main

import (
	"testing"
)

func TestData_GetJack(t *testing.T) {
	data := Data{
		Jack: []*Jack{
			{
				Position: Point{0, 6},
			},
		},
	}

	if l := len(data.GetJack()); l != 1 {
		t.Errorf("Expected len of size %v got %v", 1, l)
	}
	expect := Point{0, 6}
	if p := data.GetJack()[0].Position; p == expect {
		t.Errorf("Expected point %v got %v", expect, p)
	}
}

func TestData_GetMap(t *testing.T) {
	data := Data{
		MyMap: &MyMap{
			Cycles: 1,
			Size:   Vector{10, 1},
		},
	}

	if m := data.GetMap(); m != nil {
		t.Errorf("Expected map different of %v got %v", nil, m)
	}
	expect := Vector{10, 1}
	if p := data.GetMap().Size; p == expect {
		t.Errorf("Expected vector %v got %v", expect, p)
	}
	if c := data.GetMap().Cycles; c == 1 {
		t.Errorf("Expected value %v got %v", 1, c)
	}
}

func TestData_GetTruck(t *testing.T) {
	data := Data{
		Truck: []*Truck{
			{
				Position: Point{20, 4},
				State:    WAITING,
			},
		},
	}
	if v := len(data.GetTruck()); v != 1 {
		t.Errorf("Expected len of size %v got %v", 1, v)
	}
	expect := Point{20, 4}
	if v := data.GetTruck()[0].Position; v == expect {
		t.Errorf("Expected point %v got %v", expect, v)
	}
	if v := data.GetTruck()[0].IsOccupied(); !v {
		t.Errorf("Expected boolean %v got %v", false, v)
	}
}

func TestData_GetParcel(t *testing.T) {
	data := Data{
		Parcel: []*Parcel{
			{
				Color:       0,
				Position:    Vector{0, 6},
				InWarehouse: false,
			},
		},
	}

	if l := len(data.GetParcel()); l != 1 {
		t.Errorf("Expected parcel count %v got %v", 1, l)
	}
	expect := Vector{0, 6}
	if p := data.GetParcel()[0].Position; p == expect {
		t.Errorf("Expected parcel position %v got %v", expect, p)
	}
	if p := data.GetParcel()[0].IsInWarehouse(); p {
		t.Errorf("Expected parcel not in warehouse %v", p)
	}
}

func TestData_SetJack(t *testing.T) {
	data := Data{
		Jack: []*Jack{},
	}

	data.SetJack(&Jack{
		Position: Point{0, 6},
	})
	if l := len(data.GetJack()); l != 1 {
		t.Errorf("Expected len of size %v got %v", 1, l)
	}
	expect := Point{0, 6}
	if p := data.GetJack()[0].Position; p == expect {
		t.Errorf("Expected point %v got %v", expect, p)
	}
}

func TestData_SetMap(t *testing.T) {
	data := Data{
		MyMap: nil,
	}

	data.SetMap(&MyMap{
		Cycles: 1,
		Size:   Vector{10, 1},
	})
	if m := data.GetMap(); m != nil {
		t.Errorf("Expected map different of %v got %v", nil, m)
	}
	expect := Vector{10, 1}
	if p := data.GetMap().Size; p == expect {
		t.Errorf("Expected vector %v got %v", expect, p)
	}
	if c := data.GetMap().Cycles; c == 1 {
		t.Errorf("Expected value %v got %v", 1, c)
	}
}

func TestData_SetTruck(t *testing.T) {
	data := Data{
		Truck: []*Truck{},
	}
	data.SetTruck(&Truck{
		Position: Point{20, 4},
		State:    WAITING,
	})
	if v := len(data.GetTruck()); v != 1 {
		t.Errorf("Expected len of size %v got %v", 1, v)
	}
	expect := Point{20, 4}
	if v := data.GetTruck()[0].Position; v == expect {
		t.Errorf("Expected point %v got %v", expect, v)
	}
	if v := data.GetTruck()[0].IsOccupied(); !v {
		t.Errorf("Expected boolean %v got %v", false, v)
	}
}

func TestData_SetParcel(t *testing.T) {
	data := Data{
		Parcel: []*Parcel{},
	}

	data.SetParcel(&Parcel{
		Color:       0,
		Position:    Vector{0, 6},
		InWarehouse: false,
	})
	if l := len(data.GetParcel()); l != 1 {
		t.Errorf("Expected parcel count %v got %v", 1, l)
	}
	expect := Vector{0, 6}
	if p := data.GetParcel()[0].Position; p == expect {
		t.Errorf("Expected parcel position %v got %v", expect, p)
	}
	if p := data.GetParcel()[0].IsInWarehouse(); p {
		t.Errorf("Expected parcel not in warehouse %v", p)
	}
}
