package main

import (
	"fmt"
)

type Engine struct {
	warehouse      Point
	ticks          uint
	jackJourneys   []jackerJourney
	parcels        []parcellerStat
	dockers        []Docker
	parcelsTaken   int
	parcelsDropped int
}

type jackerJourney struct {
	jacker Jacker

	linkedDocker    *Docker
	linkedParceller *parcellerStat
	hasDestination  bool
}

type parcellerStat struct {
	parcel         Parceller
	isAvailable    bool
	hasBeenTaken   bool
	hasBeenDropped bool
}

func MakeEngine(warehouseSize Point) Engine {
	return Engine{
		warehouse:    warehouseSize,
		ticks:        0,
		jackJourneys: make([]jackerJourney, 0),
		parcels:      make([]parcellerStat, 0),
		dockers:      make([]Docker, 0),
	}
}

func (e *Engine) AddJacker(jack Jacker) {
	e.jackJourneys = append(e.jackJourneys, jackerJourney{
		jacker:          jack,
		linkedDocker:    nil,
		linkedParceller: nil,
	})
}

func (e *Engine) AddParcel(parcel Parceller) {
	e.parcels = append(e.parcels, parcellerStat{
		parcel:         parcel,
		hasBeenTaken:   false,
		hasBeenDropped: false,
		isAvailable:    true,
	})
}

func (e *Engine) AddDocker(docker Docker) {
	e.dockers = append(e.dockers, docker)
}

func (e *Engine) SetWarehouseSize(warehouse Point) {
	e.warehouse = warehouse
}

func (e *Engine) SetNbTicks(number uint) {
	e.ticks = number
}

func (e Engine) Run() error {

	for ; e.ticks > 0; {
		if e.parcelsDropped == len(e.parcels) {
			break
		}
		// Fill the empty jacks
		for idx, journey := range e.jackJourneys {

			if !journey.jacker.IsOccupied() || journey.jacker.IsArrived() {
				e.updateJackJourney(&e.jackJourneys[idx])
			}
		}

		e.undockDockersIfNeeded()

		e.tickEveryone()
	}
	e.terminate()
	return nil
}

func (e *Engine) undockDockersIfNeeded() {
	for i, docker := range e.dockers {
		if !docker.IsDocked() || docker.IsOccupied() {
			continue
		}
		if e.parcelsDropped == len(e.parcels) {
			e.dockers[i].Undock()
			continue
		}
		if parcelIdx, _ := e.findBestParceller(docker.AvailableLoad()); parcelIdx == -1 {
			if !e.areWeWaitingAJacker(docker) {
				e.dockers[i].Undock()
			}
			continue
		}
	}
}

func (e *Engine) findBestParceller(weight uint) (idx int, ret Parceller) {
	biggestFit := uint(0)
	idx = -1

	for i, p := range e.parcels {
		if p.hasBeenTaken {
			continue
		}
		if p.parcel.GetWeight() <= weight {
			if idx < 0 {
				idx = i
				biggestFit = p.parcel.GetWeight()
				ret = p.parcel
			} else if biggestFit < p.parcel.GetWeight() {
				idx = i
				biggestFit = p.parcel.GetWeight()
				ret = p.parcel
			}
		}
	}
	return idx, ret
}

func (e *Engine) tickEveryone() {
	for idx, docker := range e.dockers {
		// Tick the docker only if it is not docked
		if !docker.IsDocked() {
			obs := e.getObstacles(docker.GetPosition())
			e.dockers[idx].Tick(&obs, e.warehouse.ToVector())
		}
	}
	for idx, journey := range e.jackJourneys {
		// Tick a jacker only if it is occupied (meaning if it goes taking a parcel or if it goes to a docker).
		if journey.jacker.IsOccupied() {
			obs := e.getObstacles(e.jackJourneys[idx].jacker.GetPosition())
			e.jackJourneys[idx].jacker.Tick(&obs, e.warehouse.ToVector())
		}
	}
	e.ticks--
}

func (e *Engine) updateJackJourney(journey *jackerJourney) {
	pos := MakePoint(journey.jacker.GetPosition())

	if journey.jacker.IsEmpty() {
		if !journey.jacker.IsOccupied() {
			// If a jack does nothing
			dockerIndex := e.FindNearestDocker(pos)
			parcelIndex := e.FindNearestParceller(pos, e.dockers[dockerIndex].AvailableLoad())

			journey.linkedDocker = &e.dockers[dockerIndex]
			journey.linkedParceller = &e.parcels[parcelIndex]
			dest := VectorFromInts((*journey.linkedParceller).parcel.GetPosition())
			journey.jacker.GoTo(&dest, PARCELER)
			journey.hasDestination = true
			journey.linkedParceller.isAvailable = false
		} else if journey.jacker.IsOccupied() && journey.jacker.IsArrived() {
			// If a jack just arrived to a parceller
			dest := VectorFromInts((*journey.linkedDocker).GetPosition())
			journey.jacker.Pick(journey.linkedParceller.parcel)
			journey.jacker.GoTo(&dest, DOCKER)
			journey.linkedParceller.hasBeenTaken = true
			e.parcelsTaken++
			journey.hasDestination = true
		}
	} else if journey.jacker.IsOccupied() && journey.jacker.IsArrived() {
		parceller := journey.jacker.Drop()
		(*journey.linkedDocker).Load(parceller.GetWeight())
		journey.linkedParceller.hasBeenDropped = true
		e.parcelsDropped++
		journey.hasDestination = false
	}
}

func (e *Engine) giveJackTargetParceller(jack *Jacker) {
	pos := MakePoint((*jack).GetPosition())
	dockerIndex := e.FindNearestDocker(pos)
	parcelIndex := e.FindNearestParceller(pos, e.dockers[dockerIndex].AvailableLoad())
	if parcelIndex == -1 {
		return
	}
	dest := VectorFromInts(e.parcels[parcelIndex].parcel.GetPosition())
	(*jack).GoTo(&dest, PARCELER)
}

func (e *Engine) FindNearestParceller(to Point, massRequired uint) int {
	ret := -1
	alreadySet := false
	latest := 0.

	for idx, p := range e.parcels {
		if !p.isAvailable {
			continue
		}
		if p.parcel.GetWeight() > massRequired {
			continue
		}
		pos := MakePoint(p.parcel.GetPosition())
		if !alreadySet {
			ret = idx
			latest = pos.DistanceTo(to)
			alreadySet = true
			continue
		}
		if dist := pos.DistanceTo(to); dist < latest {
			latest = dist
			ret = idx
		}
	}

	return ret
}

func (e *Engine) FindNearestDocker(to Point) int {
	ret := -1
	alreadySet := false
	latest := 0.

	for idx, dock := range e.dockers {
		pos := MakePoint(dock.GetPosition())

		if !alreadySet {
			ret = idx
			alreadySet = true
			latest = pos.DistanceTo(to)
		}
		if dist := pos.DistanceTo(to); dist < latest {
			latest = dist
			ret = idx
		}
	}
	return ret
}

func (e *Engine) getObstacles(omitX, omitY uint) []Vector {
	ret := make([]Vector, 0, len(e.parcels)+len(e.dockers)+len(e.jackJourneys)-1)
	for _, journey := range e.jackJourneys {
		pt := MakePoint(journey.jacker.GetPosition())
		if pt.X != omitX || pt.Y != omitY {
			ret = append(ret, pt.ToVector())
		}
	}

	for _, docker := range e.dockers {
		pt := MakePoint(docker.GetPosition())
		if pt.X != omitX || pt.Y != omitY {
			ret = append(ret, pt.ToVector())
		}
	}

	for _, parceller := range e.parcels {
		if parceller.hasBeenTaken {
			continue
		}
		pt := MakePoint(parceller.parcel.GetPosition())
		if pt.X != omitX || pt.Y != omitY {
			ret = append(ret, pt.ToVector())
		}
	}
	return ret
}

func (e *Engine) terminate() {
	if e.parcelsDropped == len(e.parcels) {
		fmt.Println("😎")
	} else {
		fmt.Println("🙂")
	}
}

func (e *Engine) areWeWaitingAJacker(docker Docker) bool {
	for _, j := range e.jackJourneys {
		tp := MakePoint((*j.linkedDocker).GetPosition())
		dp := MakePoint(docker.GetPosition())
		if tp == dp && j.linkedParceller != nil && !j.linkedParceller.hasBeenDropped {
			return true
		}
	}
	return false
}
