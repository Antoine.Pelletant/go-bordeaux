package main

import (
	"math"
	"testing"
)

type testVectorInputFloatExpectVector struct {
	name   string
	value  Vector
	input  float64
	expect Vector
}
type testVectorInputVectorExpectVector struct {
	name   string
	value  Vector
	input  Vector
	expect Vector
}
type testVectorInputVectorExpectFloat struct {
	name   string
	value  Vector
	input  Vector
	expect float64
}

func TestVector_Add(t *testing.T) {
	tests := []testVectorInputFloatExpectVector{
		{
			"Add positive value",
			Vector{0, 0},
			10,
			Vector{10, 10},
		},
		{
			"Add negative value",
			Vector{10, 10},
			-1,
			Vector{9, 9},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Add(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestVector_AddVector(t *testing.T) {
	tests := []testVectorInputVectorExpectVector{
		{
			"Add same values",
			Vector{0, 0},
			Vector{10, 10},
			Vector{10, 10},
		},
		{
			"Add different values",
			Vector{10, 10},
			Vector{1, -1},
			Vector{11, 9},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.AddVector(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestVector_Subtract(t *testing.T) {
	tests := []testVectorInputFloatExpectVector{
		{
			"Subtract positive value",
			Vector{0, 0},
			10,
			Vector{-10, -10},
		},
		{
			"Subtract negative value",
			Vector{10, 10},
			-1,
			Vector{11, 11},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Subtract(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestVector_SubtractVector(t *testing.T) {
	tests := []testVectorInputVectorExpectVector{
		{
			"Subtract same values",
			Vector{0, 0},
			Vector{10, 10},
			Vector{-10, -10},
		},
		{
			"Subtract different values",
			Vector{10, 10},
			Vector{1, -1},
			Vector{9, 11},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.SubtractVector(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestVector_Multiply(t *testing.T) {
	tests := []testVectorInputFloatExpectVector{
		{
			"Multiply positive value",
			Vector{1, 1},
			10,
			Vector{10, 10},
		},
		{
			"Multiply negative value",
			Vector{10, 10},
			-2,
			Vector{-20, -20},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Multiply(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestVector_MultiplyVector(t *testing.T) {
	tests := []testVectorInputVectorExpectVector{
		{
			"Multiply same values",
			Vector{10, 0},
			Vector{10, 10},
			Vector{100, 0},
		},
		{
			"Multiply different values",
			Vector{10, 10},
			Vector{0, -1},
			Vector{0, -10},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.MultiplyVector(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestVector_Divide(t *testing.T) {
	tests := []testVectorInputFloatExpectVector{
		{
			"Divide positive value",
			Vector{1, 1},
			10,
			Vector{0.1, 0.1},
		},
		{
			"Divide negative value",
			Vector{10, 10},
			-2,
			Vector{-5, -5},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Divide(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestVector_DivideVector(t *testing.T) {
	tests := []testVectorInputVectorExpectVector{
		{
			"Divide same values",
			Vector{10, 0},
			Vector{10, 10},
			Vector{1, 0},
		},
		{
			"Divide different values",
			Vector{10, 10},
			Vector{1, -1},
			Vector{10, -10},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.DivideVector(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestVector_DotProduct(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"Dot product on same values",
			Vector{5, 5},
			Vector{10, 10},
			100,
		},
		{
			"Dot product on different values",
			Vector{3, 4},
			Vector{1, -1},
			-1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.DotProduct(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_CrossProduct(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"Cross product on same values",
			Vector{5, 5},
			Vector{10, 10},
			0,
		},
		{
			"Cross product on different values",
			Vector{3, 4},
			Vector{1, -1},
			-7,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.CrossProduct(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_Length(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"Cross product on same values",
			Vector{5, 5},
			Vector{},
			7.0710678118654755,
		},
		{
			"Cross product on different values",
			Vector{3, 4},
			Vector{},
			5,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.Length()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_Normalize(t *testing.T) {
	tests := []testVectorInputVectorExpectVector{
		{
			"Normalize on same values",
			Vector{5, 5},
			Vector{},
			Vector{0.7071067811865475, 0.7071067811865475},
		},
		{
			"Normalize on different values",
			Vector{3, 4},
			Vector{},
			Vector{0.6, 0.8},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Normalize()
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}
func TestVector_Min(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"Min on same values",
			Vector{5, 5},
			Vector{},
			5,
		},
		{
			"Min on different values",
			Vector{3, 4},
			Vector{},
			3,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.Min()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_Max(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"Cross product on same values",
			Vector{5, 5},
			Vector{},
			5,
		},
		{
			"Cross product on different values",
			Vector{3, 4},
			Vector{},
			4,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.Max()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_ToAngleRad(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleRad product on same values",
			Vector{5, 5},
			Vector{},
			math.Pi / 4,
		},
		{
			"ToAngleRad product on negative vertical",
			Vector{0, -4},
			Vector{},
			math.Pi / -2,
		},
		{
			"ToAngleRad product on positive horizontal",
			Vector{4, 0},
			Vector{},
			0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleRad()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_ToAngleDeg(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleDeg product on same values",
			Vector{5, 5},
			Vector{},
			45,
		},
		{
			"ToAngleDeg product on negative vertical",
			Vector{0, -4},
			Vector{},
			-90,
		},
		{
			"ToAngleDeg product on positive horizontal",
			Vector{4, 0},
			Vector{},
			0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleDeg()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_ToAngleDegAbs(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Vector{5, 5},
			Vector{},
			45,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Vector{0, -4},
			Vector{},
			270,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Vector{-4, -4},
			Vector{},
			225,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleDegAbs()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_AngleToRad(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Vector{5, 5},
			Vector{0, 0},
			-math.Pi / 4 * 3,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Vector{0, -3},
			Vector{0, -4},
			-math.Pi / 2,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Vector{-4, -4},
			Vector{6, 6},
			math.Pi / 4,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToRad(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestVector_AngleToDeg(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Vector{5, 5},
			Vector{0, 0},
			-135,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Vector{0, -4},
			Vector{0, -3},
			90,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Vector{-4, -4},
			Vector{6, 6},
			45,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToDeg(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}


func TestVector_AngleToDegAbs(t *testing.T) {
	tests := []testVectorInputVectorExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Vector{5, 5},
			Vector{0, 0},
			225,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Vector{0, -4},
			Vector{0, -3},
			90,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Vector{-4, -4},
			Vector{6, 6},
			45,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToDegAbs(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}
