package main

type Data struct {
	Truck  []*Truck
	Jack   []*Jack
	Parcel []*Parcel
	MyMap  *MyMap
}

func NewData(jack []*Jack, parcel []*Parcel, truck []*Truck) Data {
	return Data{
		Truck:  truck,
		Jack:   jack,
		Parcel: parcel,
		MyMap:  nil,
	}
}

func (d Data) GetJack() []*Truck {
	return d.Truck
}

func (d Data) GetTruck() []*Jack {
	return d.Jack
}

func (d Data) GetParcel() []*Parcel {
	return d.Parcel
}

func (d Data) GetMap() *MyMap {
	return d.MyMap
}

func (d *Data) SetTruck(truck *Truck) {
	d.Truck = append(d.Truck, truck)
}

func (d *Data) SetJack(jack *Jack) {
	d.Jack = append(d.Jack, jack)
}

func (d *Data) SetParcel(parcel *Parcel) {
	d.Parcel = append(d.Parcel, parcel)
}

func (d *Data) SetMap(mapd *MyMap) {
	d.MyMap = mapd
}
