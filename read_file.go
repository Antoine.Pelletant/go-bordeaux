package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func isMap(words []string) int {
	if len(words) != 3 {
		return -1
	}
	_, erra := strconv.ParseInt(words[0], 10, 64)
	_, errb := strconv.ParseInt(words[1], 10, 64)
	c, errc := strconv.ParseInt(words[2], 10, 64)
	if erra != nil || errb != nil || errc != nil {
		return -1
	}
	if c < 10 || c > 10000 {
		return -1
	}
	return 0
}

func newMyMap(words []string) *MyMap {
	a, _ := strconv.ParseInt(words[0], 10, 64)
	b, _ := strconv.ParseInt(words[1], 10, 64)
	c, _ := strconv.ParseInt(words[2], 10, 64)
	var p MyMap = NewMyMap(c, VectorFromInt(a, b))
	return &p
}

func isParcel(words []string) int {
	if len(words) != 4 {
		return -1
	}
	_, erra := strconv.ParseInt(words[1], 10, 64)
	_, errb := strconv.ParseInt(words[2], 10, 64)
	if erra != nil || errb != nil {
		return -1
	}
	color := strings.ToLower(words[3])
	if color != "green" && color != "yellow" && color != "blue" {
		return -1
	}
	return 0
}

func newParcel(words []string, m *MyMap) *Parcel {
	name := strings.ToLower(words[0])
	ai, _ := strconv.ParseInt(words[1], 10, 64)
	bi, _ := strconv.ParseInt(words[2], 10, 64)
	a, _ := strconv.ParseFloat(words[1], 64)
	b, _ := strconv.ParseFloat(words[2], 64)
	color := strings.ToLower(words[3])
	if m.GetSize().X < a || m.GetSize().Y < b {
		HandleError(ErrParcelNotInMap)
	}
	var p Parcel = NewParcel(name, VectorFromInt(ai, bi), color)
	return &p
}

func isJack(words []string) int {
	if len(words) != 3 {
		return -1
	}
	_, erra := strconv.ParseInt(words[1], 10, 64)
	_, errb := strconv.ParseInt(words[2], 10, 64)
	if erra != nil || errb != nil {
		return -1
	}
	return 0
}

func newJack(words []string, m *MyMap) *Jack {
	name := strings.ToLower(words[0])
	ai, _ := strconv.ParseInt(words[1], 10, 64)
	bi, _ := strconv.ParseInt(words[2], 10, 64)
	a, _ := strconv.ParseFloat(words[1], 64)
	b, _ := strconv.ParseFloat(words[2], 64)
	if m.GetSize().X < a || m.GetSize().Y < b {
		HandleError(ErrJackNotInMap)
	}
	var p Jack = NewJack(name, VectorFromInt(ai, bi).VectorToPoint())
	return &p
}

func isTruck(words []string) int {
	if len(words) != 4 {
		return -1
	}
	_, erra := strconv.ParseInt(words[0], 10, 64)
	_, errb := strconv.ParseInt(words[1], 10, 64)
	_, errc := strconv.ParseInt(words[2], 10, 64)
	_, errd := strconv.ParseInt(words[3], 10, 64)
	if erra != nil || errb != nil || errc != nil || errd != nil {
		return -1
	}
	return 0
}

func newTruck(words []string, m *MyMap) *Truck {
	a, _ := strconv.Atoi(words[0])
	b, _ := strconv.Atoi(words[1])
	ai, _ := strconv.Atoi(words[2])
	bi, _ := strconv.Atoi(words[3])
	if int(m.GetSize().VectorToPoint().X) < a || int(m.GetSize().VectorToPoint().Y) < b {
		HandleError(ErrTruckNotInMap)
	}
	var p Truck = NewTruck(Point{uint(a), uint(b)}, uint(ai), uint(bi))
	return &p
}

func errorEverything(a, b, c, d int) {
	if a != 1 || b < 1 || c < 1 || d != 1 {
		HandleError(ErrFile)
	}
}

func readFile(filePath string) Data {
	file, err := os.Open(filePath)

	if err != nil {
		HandleError(fmt.Sprintf("Error when opening file: %s", err))
	}
	defer file.Close()

	fileScanner := bufio.NewScanner(file)
	var i, j, k, l int
	var datas Data
	for fileScanner.Scan() {
		words := strings.Fields(fileScanner.Text())
		switch 0 {
		case isMap(words):
			if i != 0 {
				HandleError(ErrMapSizeAlreadySet)
			}
			datas.SetMap(newMyMap(words))
			i++
			break
		case isParcel(words):
			if l > 0 {
				HandleError(ErrParcelAlreadySet)
			}
			datas.SetParcel(newParcel(words, datas.GetMap()))
			k++
			break
		case isJack(words):
			if k == 0 {
				HandleError(ErrYouMustSetParcelsFirst)
			}
			datas.SetJack(newJack(words, datas.GetMap()))
			l++
			break
		case isTruck(words):
			if j != 0 {
				HandleError(ErrTruckAlreadySet)
			}
			datas.SetTruck(newTruck(words, datas.GetMap()))
			j++
			break
		default:
			HandleError(ErrFile)
		}
	}

	errorEverything(i, k, l, j)

	if err := fileScanner.Err(); err != nil {
		HandleError(fmt.Sprintf("Error while reading file: %s", err))
	}

	return datas
}
