package main

import (
	"math"
)

type Point struct {
	X, Y uint
}

func MakePoint(X, Y uint) Point {
	return Point{X, Y}
}

// This function makes a Point from two floats (used by PointToPoint)
func PointFromFloats(X, Y float64) Point {
	return Point{uint(X), uint(Y)}
}

// This function simply add a float to both coordinates of the Point v
func (p *Point) Add(a uint) {
	p.X += a
	p.Y += a
}

// This function simply add the values of another Point a for both X and Y
func (p *Point) AddPoint(a Point) {
	p.X += a.X
	p.Y += a.Y
}

// This function simply subtract a float to both coordinates of the Point v
func (p *Point) SubtractPoint(a Point) {
	p.X -= a.X
	p.Y -= a.Y
}

// This function simply subtract the values of another Point a for both X and Y
func (p *Point) Subtract(a uint) {
	p.X -= a
	p.Y -= a
}

// This function simply subtract the values of another Point a for both X and Y
func (p *Point) MultiplyPoint(a Point) {
	p.X *= a.X
	p.Y *= a.Y
}

// This function simply multiply a float to both coordinates of the Point v
func (p *Point) Multiply(a uint) {
	p.X *= a
	p.Y *= a
}

// This function simply subtract the values of another Point a for both X and Y
func (p *Point) DividePoint(a Point) {
	if a.X != 0 {
		p.X /= a.X
	}
	if a.Y != 0 {
		p.Y /= a.Y
	}
}

// This function simply divide a float to both coordinates of the Point v
func (p *Point) Divide(a uint) {
	if a != 0 {
		p.X /= a
		p.Y /= a
	}
}

// This function returns the dot product of a point
// More information about what is a dot product here https://en.wikipedia.org/wiki/Dot_product
func (p *Point) DotProduct(a Point) float64 {
	return float64(p.X*a.X + p.Y*a.Y)
}

// This function returns the cross product of a point
// More information about what is a dot product here https://en.wikipedia.org/wiki/Cross_product
func (p Point) CrossProduct(a Point) float64 {
	return float64(p.X*a.Y - p.Y*a.X)
}

// This function returns the length of a point. The length is the defined like the hypotenuse of a point
// Example for the point (3;4) :
// length = sqrt(X² + Y²)
//        = sqrt(3² + 4²)
// 		  = sqrt(25)
// length = 5
func (p Point) Length() float64 {
	return math.Sqrt(p.DotProduct(p))
}

// This function returns the angle (the slope) of the Point in radians
func (p Point) ToAngleRad() float64 {
	return -math.Atan2(float64(-p.Y), float64(p.X))
}

// This function returns the angle (the slope) of the Point in degrees
func (p Point) ToAngleDeg() float64 {
	return p.ToAngleRad() * 180 / math.Pi
}

// This function returns the angle (the slope) of the Point in degrees absolute (between 0 and 360)
func (p Point) ToAngleDegAbs() float64 {
	ret := math.Mod(p.ToAngleDeg(), 360)
	if ret < 0 {
		return ret + 360
	}
	return ret
}

// This function returns the angle (the slope) of two Points in radians
func (p Point) AngleToRad(a Point) float64 {
	a.SubtractPoint(p)
	return a.ToAngleRad()
}

// This function returns the angle (the slope) of two Points in degrees (from -180 to 180)
func (p Point) AngleToDeg(a Point) float64 {
	a.SubtractPoint(p)
	return a.ToAngleDeg()
}

// This function returns the angle (the slope) of two Points in degrees absolute (from 0 to 360)
func (p Point) AngleToDegAbs(a Point) float64 {
	a.SubtractPoint(p)
	return a.ToAngleDegAbs()
}

// This function converts a Point to a Vector
func (p Point) ToVector() Vector {
	return VectorFromInts(p.X, p.Y)
}

// This function returns the distance between the Point p and the given Point a
func (p Point) DistanceTo(a Point) float64 {
	a.SubtractPoint(p)
	return a.Length()
}
