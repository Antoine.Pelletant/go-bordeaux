### Go Bordeaux

## Build

To build the project simply run at the root of the project:
```bash
$ go build
```

## Run

The program expect one argument:
```bash
./go-bordeaux -file-path="/path/to/file"
```

## Tests

To run the tests:

```bash 
$ go test
```
