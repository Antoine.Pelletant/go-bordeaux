package main

import (
	"fmt"
)

type Status int

const (
	WAITING Status = iota
	GONE
)

type Truck struct {
	Position      Point
	MaxWeight     uint
	CurrentWeight uint
	MaxCycles     uint
	CurrentCycle  uint
	State         Status
}

func NewTruck(pos Point, maxWeight uint, maxCycles uint) Truck {
	return Truck{
		Position:      pos,
		MaxWeight:     maxWeight,
		CurrentWeight: 0,
		MaxCycles:     maxCycles,
		CurrentCycle:  0,
		State:         WAITING,
	}
}

func (t *Truck) Load(weight uint) {
	t.CurrentWeight += weight
}

func (t Truck) GetPosition() (uint, uint) {
	return t.Position.X, t.Position.Y
}

func (t *Truck) GetMaxWeight() uint {
	return t.MaxWeight
}

func (t Truck) AvailableLoad() uint {
	return t.MaxWeight - t.CurrentWeight
}

func (t *Truck) GetEtat() Status {
	return t.State
}

func (t *Truck) SetEtat(newStatus Status) {
	t.State = GONE
}

func (t Truck) PrintState() {
	fmt.Printf("camion ")
	if t.GetEtat() == GONE {
		fmt.Printf("GONE ")
	} else {
		fmt.Printf("WAITING ")
	}
	fmt.Printf("%d %d\n", t.MaxWeight, t.CurrentWeight)
}

func (t *Truck) Tick(_ *[]Vector, _ Vector) {
	t.CurrentCycle++
	t.PrintState()
}

func (t Truck) IsDocked() bool {
	if t.State == WAITING {
		return true
	}
	return false
}

func (t Truck) IsOccupied() bool {
	if t.State == GONE {
		return true
	}
	return false
}

func (t *Truck) Undock() {
	t.State = GONE
}

func (t Truck) RemainCycles() uint {
	return t.MaxCycles - t.CurrentCycle
}
