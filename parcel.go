package main

import "strings"

type Colors int

const (
	YELLOW Colors = iota
	GREEN
	BLUE
)

//Parcel implememt parcel
type Parcel struct {
	weight      uint
	Color       Colors
	Position    Vector
	name        string
	InWarehouse bool
}

//NewParcel Create a new Parcel
func NewParcel(name string, position Vector, color string) Parcel {
	var p Parcel
	color = strings.ToUpper(color)
	switch color {
	case "BLUE":
		p = Parcel{
			weight:      500,
			Color:       BLUE,
			Position:    position,
			name:        name,
			InWarehouse: true,
		}
	case "GREEN":
		p = Parcel{
			weight:      200,
			Color:       GREEN,
			Position:    position,
			name:        name,
			InWarehouse: true,
		}
	case "YELLOW":
		p = Parcel{
			weight:      100,
			Color:       YELLOW,
			Position:    position,
			name:        name,
			InWarehouse: true,
		}
	default:
		HandleError(ErrCreatingParcel)
	}
	return p
}

//IsInWarehouse is parcel in Warehouse
func (p Parcel) IsInWarehouse() bool {
	return p.InWarehouse
}

//IsInTruck is parcel in truck
func (p Parcel) IsInTruck() bool {
	return !p.InWarehouse
}

//GetWeight return parcel weight
func (p Parcel) GetWeight() uint {
	return p.weight
}

//GetColor return parcel color
func (p Parcel) GetColor() string {
	switch p.Color {
	case YELLOW:
		return "yellow"
	case GREEN:
		return "green"
	case BLUE:
		return "blue"
	default:
		HandleError(ErrColorNotFound)
	}
	// Will never get there cause HandleError panic
	return ""
}

//GetPosition return parcel position
func (p Parcel) GetPosition() (x, y uint) {
	return uint(p.Position.X), uint(p.Position.Y)
}

func (p Parcel) GetName() string {
	return p.name
}
