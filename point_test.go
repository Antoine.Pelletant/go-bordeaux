package main

import (
	"math"
	"testing"
)

type testPointInputFloatExpectPoint struct {
	name   string
	value  Point
	input  uint
	expect Point
}
type testPointInputPointExpectPoint struct {
	name   string
	value  Point
	input  Point
	expect Point
}
type testPointInputPointExpectFloat struct {
	name   string
	value  Point
	input  Point
	expect float64
}

func TestPoint_Add(t *testing.T) {
	tests := []testPointInputFloatExpectPoint{
		{
			"Add positive value",
			Point{0, 0},
			10,
			Point{10, 10},
		},
		{
			"Add negative value",
			Point{10, 10},
			-1,
			Point{9, 9},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Add(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestPoint_AddPoint(t *testing.T) {
	tests := []testPointInputPointExpectPoint{
		{
			"Add same values",
			Point{0, 0},
			Point{10, 10},
			Point{10, 10},
		},
		{
			"Add different values",
			Point{10, 10},
			Point{1, -1},
			Point{11, 9},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.AddPoint(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestPoint_Subtract(t *testing.T) {
	tests := []testPointInputFloatExpectPoint{
		{
			"Subtract positive value",
			Point{0, 0},
			10,
			Point{-10, -10},
		},
		{
			"Subtract negative value",
			Point{10, 10},
			-1,
			Point{11, 11},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Subtract(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestPoint_SubtractPoint(t *testing.T) {
	tests := []testPointInputPointExpectPoint{
		{
			"Subtract same values",
			Point{0, 0},
			Point{10, 10},
			Point{-10, -10},
		},
		{
			"Subtract different values",
			Point{10, 10},
			Point{1, -1},
			Point{9, 11},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.SubtractPoint(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestPoint_Multiply(t *testing.T) {
	tests := []testPointInputFloatExpectPoint{
		{
			"Multiply positive value",
			Point{1, 1},
			10,
			Point{10, 10},
		},
		{
			"Multiply negative value",
			Point{10, 10},
			-2,
			Point{-20, -20},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Multiply(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestPoint_MultiplyPoint(t *testing.T) {
	tests := []testPointInputPointExpectPoint{
		{
			"Multiply same values",
			Point{10, 0},
			Point{10, 10},
			Point{100, 0},
		},
		{
			"Multiply different values",
			Point{10, 10},
			Point{0, -1},
			Point{0, -10},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.MultiplyPoint(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestPoint_Divide(t *testing.T) {
	tests := []testPointInputFloatExpectPoint{
		{
			"Divide positive value",
			Point{1, 1},
			10,
			Point{0, 0},
		},
		{
			"Divide negative value",
			Point{10, 10},
			-2,
			Point{-5, -5},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Divide(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.input)
			}
		})
	}
}

func TestPoint_DividePoint(t *testing.T) {
	tests := []testPointInputPointExpectPoint{
		{
			"Divide same values",
			Point{10, 0},
			Point{10, 10},
			Point{1, 0},
		},
		{
			"Divide different values",
			Point{10, 10},
			Point{1, -1},
			Point{10, -10},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.DividePoint(test.input)
			if test.value != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value)
			}
		})
	}
}

func TestPoint_DotProduct(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"Dot product on same values",
			Point{5, 5},
			Point{10, 10},
			100,
		},
		{
			"Dot product on different values",
			Point{3, 4},
			Point{1, -1},
			-1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.DotProduct(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_CrossProduct(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"Cross product on same values",
			Point{5, 5},
			Point{10, 10},
			0,
		},
		{
			"Cross product on different values",
			Point{3, 4},
			Point{1, -1},
			-7,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.CrossProduct(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_Length(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"Cross product on same values",
			Point{5, 5},
			Point{},
			7.0710678118654755,
		},
		{
			"Cross product on different values",
			Point{3, 4},
			Point{},
			5,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.Length()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_ToAngleRad(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleRad product on same values",
			Point{5, 5},
			Point{},
			math.Pi / 4,
		},
		{
			"ToAngleRad product on negative vertical",
			Point{0, -4},
			Point{},
			math.Pi / -2,
		},
		{
			"ToAngleRad product on positive horizontal",
			Point{4, 0},
			Point{},
			0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleRad()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_ToAngleDeg(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleDeg product on same values",
			Point{5, 5},
			Point{},
			45,
		},
		{
			"ToAngleDeg product on negative vertical",
			Point{0, -4},
			Point{},
			-90,
		},
		{
			"ToAngleDeg product on positive horizontal",
			Point{4, 0},
			Point{},
			0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleDeg()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_ToAngleDegAbs(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Point{5, 5},
			Point{},
			45,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Point{0, -4},
			Point{},
			270,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Point{-4, -4},
			Point{},
			225,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.ToAngleDegAbs()
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_AngleToRad(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Point{5, 5},
			Point{0, 0},
			-math.Pi / 4 * 3,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Point{0, -3},
			Point{0, -4},
			-math.Pi / 2,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Point{-4, -4},
			Point{6, 6},
			math.Pi / 4,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToRad(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}

func TestPoint_AngleToDeg(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Point{5, 5},
			Point{0, 0},
			-135,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Point{0, -4},
			Point{0, -3},
			90,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Point{-4, -4},
			Point{6, 6},
			45,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToDeg(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}


func TestPoint_AngleToDegAbs(t *testing.T) {
	tests := []testPointInputPointExpectFloat{
		{
			"ToAngleDegAbs product on same values",
			Point{5, 5},
			Point{0, 0},
			225,
		},
		{
			"ToAngleDegAbs product on negative vertical",
			Point{0, -4},
			Point{0, -3},
			90,
		},
		{
			"ToAngleDegAbs product on full negative diagonal",
			Point{-4, -4},
			Point{6, 6},
			45,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ret := test.value.AngleToDegAbs(test.input)
			if ret != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, ret)
			}
		})
	}
}
