package main

import (
	"math"
)

var pathInc int = 1

//Path contain all the variables needed to find the path
type Path struct {
	dist Vector
	inc  Vector
	px   Vector
}

func case1(path Path) []Vector {
	var i uint = 1
	var c uint = 0

	newPath := make([]Vector, 0)
	c = path.dist.VectorToPoint().X / 2
	for i <= path.dist.VectorToPoint().X {
		path.px.X += path.inc.X
		c += path.dist.VectorToPoint().Y
		if c >= path.dist.VectorToPoint().X {
			c -= path.dist.VectorToPoint().X
			path.px.Y += path.inc.Y
		}
		newPath = append(newPath, Vector{path.px.X, path.px.Y})
		pathInc++
		i++
	}

	return newPath
}

func case2(path Path, whSize Vector) []Vector {

	var i uint = 1
	var c uint = 0
	newPath := make([]Vector, 0)

	c = path.dist.VectorToPoint().Y / 2
	for i <= path.dist.VectorToPoint().Y {
		path.px.Y += path.inc.Y
		c += path.dist.VectorToPoint().X
		if c >= path.dist.VectorToPoint().Y {
			c -= path.dist.VectorToPoint().Y
			path.px.X += path.inc.X
		}
		if path.px.VectorToPoint().X > uint(whSize.X) || path.px.VectorToPoint().Y > uint(whSize.Y) {
			break
		} else {
			newPath = append(newPath, Vector{path.px.X, path.px.Y})
			pathInc++
		}
		i++
	}
	return newPath
}

func clearAliasing(path []Vector, origin Vector) []Vector {
	ret := make([]Vector, 0)
	var last Vector = origin
	for i, p := range path {
		if p.X != last.X && p.Y != last.Y || i == 0 {
			ret = append(ret, Vector{last.X, p.Y})
		}
		ret = append(ret, p)
		last = p
	}
	return ret
}

func getInc(v1, v2 uint) int {
	if v1 > v2 {
		return 1
	}
	return -1
}

//Getpath return the complete path to go to a define point
func Getpath(whSize Vector, start Vector, end Vector) []Vector {
	var path Path
	var pos []Vector

	path.dist.X = math.Abs(float64(end.X) - float64(start.X))
	path.dist.Y = math.Abs(float64(end.Y) - float64(start.Y))
	path.inc.X = float64(getInc(end.VectorToPoint().X, start.VectorToPoint().X))
	path.inc.Y = float64(getInc(end.VectorToPoint().Y, start.VectorToPoint().Y))
	path.px.X = start.X
	path.px.Y = start.Y

	if path.dist.VectorToPoint().X > path.dist.VectorToPoint().Y {
		pos = case1(path)
	} else {
		pos = case2(path, whSize)
	}
	return clearAliasing(pos, start)
}

//GetNextPos return the next position where the jack has to go to
func GetNextPos(whSize Vector, startPos Vector, endPos Vector) *Vector {
	pos := Getpath(whSize, startPos, endPos)

	if len(pos) > 1 {
		if pos[0].X == startPos.X && pos[0].Y == startPos.Y {
			return &pos[1]
		}
		return &pos[0]
	}
	return nil
}
