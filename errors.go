package main

import (
	"fmt"
	"os"
)

var (
	ErrCreatingParcel    = fmt.Errorf("Error Creating Parcel")
	ErrColorNotFound     = fmt.Errorf("Color not found")
	ErrParcelNotInMap    = "Le coli doit être dans la map"
	ErrJackNotInMap      = "Le transpalette doit être dans la map"
	ErrTruckNotInMap     = "Le camion doit être dans la map"
	ErrFile              = "erreur fichier"
	ErrMapSizeAlreadySet = "Les informations sur la map doivent être fournit qu'une seule fois"
	ErrParcelAlreadySet  = "Vous ne pouvez plus déclarer de colis"
	ErrYouMustSetParcelsFirst  = "Vous devez déclarer les colis en premier"
	ErrTruckAlreadySet  = "Les informations sur le camion doivent être fournit qu'une seul fois"
)

func HandleError(v interface{}) {
	_, _ = fmt.Fprintln(os.Stderr, "😱")
	panic(v)
}
