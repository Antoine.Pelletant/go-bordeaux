package main

import (
	"flag"
)

func main() {

	file := flag.String("file-path", "/path/to/file", "must be a path")

	flag.Parse()

	data := readFile(*file)

	engine := MakeEngine(data.MyMap.GetSize().VectorToPoint())
	for _, v := range data.Jack {
		engine.AddJacker(v)
	}

	engine.SetNbTicks(uint(data.MyMap.Cycles))

	for _, v := range data.Parcel {
		engine.AddParcel(v)
	}

	for _, v := range data.Truck {
		engine.AddDocker(v)
	}

	_ = engine.Run()
}
