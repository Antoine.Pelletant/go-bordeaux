package main

import (
	"testing"
)

type testTrucksLoad struct {
	name   string
	value  Truck
	expect uint
}

type TestTrucksAddLoad struct {
	name     string
	value    Truck
	input    uint
	expected uint
}

func TestTrucks_AvailableLoad(t *testing.T) {
	tests := []testTrucksLoad{
		{
			"Test 800 available",
			Truck{Point{1, 1}, 1000, 200, 10, 0, WAITING},
			800,
		},
		{
			"Test 600 available",
			Truck{Point{1, 1}, 1000, 400, 10, 0, WAITING},
			600,
		},
		{
			"Test 100 available",
			Truck{Point{1, 1}, 1000, 900, 10, 0, WAITING},
			100,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if test.value.AvailableLoad() != test.expect {
				t.Errorf("Test failed expecting %v got %v", test.expect, test.value.AvailableLoad())
			}
		})
	}
}

func TestTruck_AddLoad(t *testing.T) {
	tests := []TestTrucksAddLoad{
		{
			"Test add 100 available",
			Truck{Point{1, 1}, 1000, 200, 10, 0, WAITING},
			100,
			700,
		},
		{
			"Test add 200 available",
			Truck{Point{1, 1}, 1000, 400, 10, 0, WAITING},
			200,
			400,
		},
		{
			"Test add 500 available",
			Truck{Point{1, 1}, 1000, 400, 10, 0, WAITING},
			500,
			100,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.value.Load(test.input)
			if test.value.AvailableLoad() != test.expected {
				t.Errorf("Test failed expecting %v got %v", test.expected, test.value.AvailableLoad())
			}
		})
	}
}
