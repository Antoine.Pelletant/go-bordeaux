package main

// Un truck est Ticker et Trucker
// Un jack est Ticker et Jacker
type Ticker interface {
	Tick(obstacles *[]Vector, wareHouseSize Vector)
	IsOccupied() bool
}

type Positionner interface {
	GetPosition() (x, y uint)
}

// Un parcel est Parceller
type Parceller interface {
	Positionner
	IsInTruck() bool
	IsInWarehouse() bool
	GetWeight() uint
	GetColor() string
	GetName() string
}

// A truck is Docker
type Docker interface {
	Ticker
	Positionner
	Load(weight uint)
	AvailableLoad() (available uint)
	RemainCycles() (remain uint)
	IsDocked() bool
	Undock()
}

// A Jack is Jacker
type Jacker interface {
	Ticker
	Positionner
	GoTo(destination *Vector, goToType goToType)
	Pick(parcel Parceller)
	Drop() Parceller
	IsArrived() bool
	IsEmpty() bool
}
